﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocumentaryManagementSystem.Models
{
    public class Actor
    {

        public int ActorId { get; set; }
        public String ActorName { get; set; }
        public int ActorAge { get; set; }
        public String ActorGender { get; set; }
        public String ActorDOB { get; set; }
        public virtual ICollection<Documentary> Documentaries { get; set; }
    }
}
