﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocumentaryManagementSystem.Models
{
    public class Documentary
    {

        public int DocumentaryId { get; set; }
        public String DocumentaryName { get; set; }
        public int GenreId { get; set; }
        public Genre Genre { get; set; }
        public int ActorId { get; set; }
        //public int Xyz { get; set; }
        public virtual Actor Actors { get; set; }

        public static explicit operator Documentary(List<object> v)
        {
            throw new NotImplementedException();
        }
    }
}
