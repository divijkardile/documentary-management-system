﻿using DocumentaryManagementSystem.Models;
using DocumentaryManagementSystem.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocumentaryManagementSystem.Service
{
    public class DocumentaryService : IDocumentaryService
    {

        private readonly IDocumentaryRepo _documentaryRepo;

        public DocumentaryService(IDocumentaryRepo documentaryRepo)
        {

            _documentaryRepo = documentaryRepo;
        }

        public async Task<int> AddActor(Actor actor)
        {

            return await _documentaryRepo.AddActor(actor);
        }

        public async Task<int> AddDocumentary(Documentary documentary)
        {

            return await _documentaryRepo.AddDocumentary(documentary);
        }

        public async Task<dynamic> GetAllDocumentaryWithActor()
        {

            return await _documentaryRepo.GetAllDocumentaryWithActor();
        }

        public async Task<List<Documentary>> GetDocumentaryByActorId(int actorId)
        {

            return await _documentaryRepo.GetDocumentaryByActorId(actorId);
        }
    }
}
