﻿using DocumentaryManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocumentaryManagementSystem.Repository
{
    public interface IDocumentaryRepo
    {

        public Task<int> AddActor(Actor actor);

        public Task<int> AddDocumentary(Documentary documentary);

        public Task<List<Documentary>> GetDocumentaryByActorId(int actorId);

        public Task<dynamic> GetAllDocumentaryWithActor();
    }
}
