﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DocumentaryManagementSystem.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DocumentaryManagementSystem.Repository
{
    public class ActorEntityConfiguration : IEntityTypeConfiguration<Actor>
    {
        public void Configure(EntityTypeBuilder<Actor> builder)
        {

            builder.HasKey(p => p.ActorId);

            builder.Property(a => a.ActorId)
                .UseIdentityColumn();

            builder.HasMany(a => a.Documentaries)
                .WithOne(p => p.Actors).HasForeignKey(e => e.DocumentaryId).IsRequired(true);
        }
    }
}
