﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DocumentaryManagementSystem.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DocumentaryManagementSystem.Repository
{
    public class DocumentaryEntityConfiguration : IEntityTypeConfiguration<Documentary>
    {
        public void Configure(EntityTypeBuilder<Documentary> builder)
        {

            builder.HasKey(d => d.DocumentaryId);

            builder.Property(d => d.DocumentaryId)
                .UseIdentityColumn();

            builder.Property(d => d.DocumentaryName)
                .HasMaxLength(50)
                .IsRequired();

            builder.HasOne(d => d.Genre)
                .WithMany()
                .HasForeignKey(f => f.GenreId)
                .IsRequired(true);

            builder.HasOne(p => p.Actors)
                .WithMany(e => e.Documentaries)
                .IsRequired(true)
                .HasForeignKey(e => e.ActorId);
        }
    }
}
