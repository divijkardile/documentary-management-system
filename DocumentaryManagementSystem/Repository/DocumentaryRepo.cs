﻿using DocumentaryManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using DocumentaryManagementSystem.CustomExceptions;
using Microsoft.EntityFrameworkCore;

namespace DocumentaryManagementSystem.Repository
{
    public class DocumentaryRepo : IDocumentaryRepo
    {

        private readonly DocumentaryDbContext _documentaryDbContext;

        public DocumentaryRepo(DocumentaryDbContext documentaryDbContext)
        {

            _documentaryDbContext = documentaryDbContext;
        }

        public async Task<int> AddActor(Actor actor)
        {
            try
            {

                _documentaryDbContext.Actors.Add(actor);
                await _documentaryDbContext.SaveChangesAsync();

                return actor.ActorId;
            }
            catch (SqlException ex)
            {

                throw new SqlCustomException("Can't connect to Database!!!", ex);
            }
        }

        public async Task<int> AddDocumentary(Documentary documentary)
        {

            try
            {

                if (DocumentaryExists(documentary.DocumentaryName))
                {

                    throw new DuplicateDocumentaryNameException("Documentary name already present!!!");
                }

                _documentaryDbContext.Documentaries.Add(documentary);
                await _documentaryDbContext.SaveChangesAsync();

                return documentary.DocumentaryId;
            }
            catch (SqlException ex)
            {

                throw new SqlCustomException("Can't connect to Database!!!", ex);
            }
        }

        public async Task<dynamic> GetAllDocumentaryWithActor()
        {

            try
            {

                var data = await _documentaryDbContext.Documentaries.Select(p => new
                {
                    p.DocumentaryName,
                    p.Genre.GenreName,
                    p.Actors.ActorName,
                    p.Actors.ActorAge,
                    p.Actors.ActorDOB
                }).ToListAsync();

                return data;
            }
            catch (SqlException ex)
            {

                throw new SqlCustomException("Can't connect to Database!!!", ex);
            }
        }

        public async Task<List<Documentary>> GetDocumentaryByActorId(int actorId)
        {

            try
            {

                var data = await _documentaryDbContext.Documentaries.Where(p => p.ActorId == actorId).ToListAsync();

                return data;
            }
            catch (SqlException ex)
            {

                throw new SqlCustomException("Can't connect to Database!!!", ex);
            }
        }

        private bool DocumentaryExists(string documentaryName)
        {
            return _documentaryDbContext.Documentaries.Any(e => e.DocumentaryName == documentaryName);
        }
    }
}
