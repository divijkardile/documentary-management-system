﻿using DocumentaryManagementSystem.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocumentaryManagementSystem.Repository
{
    public class DocumentaryDbContext : DbContext
    {

        public DocumentaryDbContext()
        {

        }

        public DocumentaryDbContext(DbContextOptions<DocumentaryDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.ApplyConfiguration(new ActorEntityConfiguration());
            modelBuilder.ApplyConfiguration(new DocumentaryEntityConfiguration());

            modelBuilder.Entity<Genre>().HasData( 
                new Genre
                {
                    GenreId = 1,
                    GenreName = "Horror"
                },
                new Genre
                {
                    GenreId = 2,
                    GenreName = "Comedy"
                }
            );
        }

        public DbSet<Actor> Actors { get; set; }
        public DbSet<Documentary> Documentaries { get; set; }
    }
}
