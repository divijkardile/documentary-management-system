﻿using DocumentaryManagementSystem.Models;
using DocumentaryManagementSystem.Service;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocumentaryManagementSystem.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class ActorDocumentaryController : ControllerBase
    {

        private readonly IDocumentaryService _documentaryService;

        public ActorDocumentaryController(IDocumentaryService documentaryService)
        {

            _documentaryService = documentaryService;
        }

        [HttpPost("addactor")]
        public async Task<ActionResult> AddActor(Actor actor)
        {

            try
            {

                int rowAffect = await _documentaryService.AddActor(actor);

                if (rowAffect > 0)
                {

                    return Ok("Data inserted sucessfully");
                }
                else
                {

                    return NotFound("Failed to insert data.");
                }
            }
            catch(Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }
        
        [HttpPost("adddocumentary")]
        public async Task<ActionResult> AddDocumentary(Documentary documentary)
        {

            try
            {

                int rowAffect = await _documentaryService.AddDocumentary(documentary);

                if (rowAffect > 0)
                {

                    return Ok("Data inserted sucessfully");
                }
                else
                {

                    return NotFound("Failed to insert data.");
                }
            }
            catch(Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [HttpGet("getdocumentarybyactorid")]
        public async Task<List<Documentary>> GetDocumentaryByActorId(int actorId)
        {

            try
            {

                var data = await _documentaryService.GetDocumentaryByActorId(actorId);

                return data;
            }
            catch (Exception)
            {

                return null;
            }
        }

        [HttpGet]

        public async Task<dynamic> GetAll()
        {

            var data = await _documentaryService.GetAllDocumentaryWithActor();

            return Ok(data);
        }
    }
}
