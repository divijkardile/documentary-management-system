﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocumentaryManagementSystem.CustomExceptions
{
    public class DetailsNotFoundException : Exception
    {

        public DetailsNotFoundException(String message) :  base (message)
        {
            
        }
    }
}
