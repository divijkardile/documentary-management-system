﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocumentaryManagementSystem.CustomExceptions
{
    public class DuplicateDocumentaryNameException : Exception
    {

        public DuplicateDocumentaryNameException(string message) : base(message)
        {

        }
    }
}
